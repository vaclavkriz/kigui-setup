const { dbUser, dbPassword } = require('./credentials.json');

module.exports = {
    mongoURI: `mongodb+srv://${dbUser}:${dbPassword}@dbki-gui-ozqso.mongodb.net/test?retryWrites=true`
}